
all: xkrizv01.zip

doc.pdf: doc/proj2.pdf
	cp doc/proj2.pdf doc.pdf

doc/proj2.pdf: doc/proj2.tex
	make -C doc

FILES= \
	src/README.txt \
	src/main.py \
	src/approx.py \
	src/exact.py \
	src/graph.py \
	src/test.py \
	src/utils.py \
	src/xhloze01.py \
	src/generate.sh

xkrizv01.zip: doc.pdf ${FILES}
	zip xkrizv01.zip doc.pdf ${FILES}
