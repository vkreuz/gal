#!/usr/bin/env python3
# Vitezslav Kriz, xkrizv01

import exact
import approx
from utils import load_net, run_once
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE")
    args = parser.parse_args()
    file = args.FILE
    net = load_net(file)
    net.convert_to_adjlist()
    flow1, elapsed1 = run_once(net, exact.exact_flow)
    print("Exact algorithm: Flow: {} Elapsed Time {}".format(flow1, elapsed1))
    flow2, elapsed2 = run_once(net, approx.approx)
    print("Aprox algorithm: Flow: {} Elapsed Time {}".format(flow2, elapsed2))
    print("Ratio: {}, SpeedUp: {}".format(flow1/flow2, elapsed1/elapsed2))
