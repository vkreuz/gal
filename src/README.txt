

Data se vygeneruji pomoci shell scriptu generate.sh
$ bash generate.sh
Pote se spusti vybrane testy pomoci
$ python3 test.py

Pokud je treba spustit test jen na jednou siti je mozne vyuzit program main.py
$ python3 main.py nazev_site.csv

Pro vygenerovani site lze vyuzit xhloxe01.py napriklad takto:

$ python3 xhloze01.py -w -wmin 1 -wmax 30 -f csv -s -d -r 0.95 -n 500 > nazev_site.csv

