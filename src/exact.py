# Vitezslav Kriz, xkrizv01

import math
from graph import Graph, Net
import copy


def bfs(graph: Graph, source, sink):
    color = ["WHITE" for _ in graph.vertices]
    d = [math.inf for _ in graph.vertices]
    pi = [None for _ in graph.vertices]
    color[source] = "WHITE"
    d[source] = 0
    pi[source] = None
    queue = [source]
    while queue:
        u = queue.pop()
        if graph.converted:
            row = graph.adj[u].items()
        else:
            row = enumerate(graph.adj[u])
        for v, cap in row:
            if cap > 0 and color[v] == "WHITE":
                color[v] = "GRAY"
                d[v] = d[u] + 1
                pi[v] = u
                queue.append(v)
                if v == sink:  # this can reduce complexity
                    return pi
        color[u] = "BLACK"

    return False


def exact_flow(net: Net):  # O(n^4)
    f = [[0 for _ in range(net.size)] for _ in range(net.size)]  # O(n)

    while True:  # O(n)
        pi = bfs(net, net.source, net.sink)  # O(n^3)
        if not pi:  # no path in residual network -> stop
            break
        u = net.sink
        cf = math.inf
        while u != net.source:
            cf = min(cf, net.adj[pi[u]][u])
            u = pi[u]

        u = net.sink
        while u != net.source:
            f[pi[u]][u] = f[pi[u]][u] + cf
            f[u][pi[u]] = -f[pi[u]][u]
            net.adj[pi[u]][u] -= cf
            u = pi[u]

    return sum(f[net.source])


def exact_flow_limited(net: Net, limit):  # O(n^4)
    f = [[0 for _ in range(net.size)] for _ in range(net.size)]  # O(n)

    for _ in range(limit):  # O(n)
        pi = bfs(net, net.source, net.sink)  # O(n^3)
        if not pi:  # no path in residual network -> stop
            break
        u = net.sink
        cf = math.inf
        while u != net.source:
            cf = min(cf, net.adj[pi[u]][u])
            u = pi[u]

        u = net.sink
        while u != net.source:
            f[pi[u]][u] = f[pi[u]][u] + cf
            f[u][pi[u]] = -f[pi[u]][u]
            net.adj[pi[u]][u] -= cf
            u = pi[u]

    return sum(f[net.source])
