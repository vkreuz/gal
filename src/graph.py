# Vitezslav Kriz, xkrizv01

class Graph:
    def __init__(self, adj_matrix):
        self.adj = adj_matrix
        self.converted = False
        self._count = None

    @classmethod
    def empty(cls, size, adjlist=False):
        if adjlist:
            return cls([dict() for _ in range(size)])
        else:
            return cls([[0 for _ in range(size)] for _ in range(size)])

    @property
    def vertices(self):
        return range(len(self.adj))

    @property
    def size(self):
        return len(self.adj)

    def for_edges(self, cb):
        for u in range(self.size):
            if self.converted:
                row = self.adj[u].items()
            else:
                row = enumerate(self.adj[u])
            for v, cap in row:
                if cap > 0:
                    retval = cb(u, v, cap)
                    if retval is not None:
                        self.adj[u][v] = retval

    def edge_count(self):
        def counter(u, v, cap):
            counter.c += 1
        counter.c = 0
        self.for_edges(counter)
        return counter.c

    def convert_to_adjlist(self):
        adj_list = []
        for u in range(self.size):
            adj_row = dict()
            for v in range(self.size):
                cap = self.adj[u][v]
                if cap > 0:
                    adj_row[v] = cap
            adj_list.append(adj_row)
        self.adj = adj_list
        self.converted = True


class Net(Graph):
    def __init__(self, adj_matrix, source=0, sink=None):
        super().__init__(adj_matrix)
        if sink is None:
            self.sink = len(adj_matrix) - 1
        else:
            self.sink = sink
        self.source = source
