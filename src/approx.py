# Vitezslav Kriz, xkrizv01

from graph import Graph, Net
import random
import exact
import math

random.seed()


def sparsify(net: Net, p):
    def inner(u, v, cap):
        if random.random() < p:
            return cap
        else:
            return 0
    net.for_edges(inner)
    return net


def approx(net: Net):
    sparsify(net, 0.6)
    return exact.exact_flow(net)
