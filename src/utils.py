# Vitezslav Kriz, xkrizv01

import time
import graph
import copy
import csv


def load_net(file):
    with open(file, "r") as f:
        return graph.Net(list(map(lambda l: list(map(int, l)), csv.reader(f))))


def run_once(net, alg):
    net_copy = copy.deepcopy(net)
    start = time.process_time()
    result = alg(net_copy)
    elapsed_time = time.process_time() - start
    return result, elapsed_time
