#!/usr/bin/env python3
# Vitezslav Kriz, xkrizv01

import exact
import copy
import approx
from utils import load_net, run_once
import csv
import datasets


def run(files, alg, out):
    writer = csv.writer(out)
    writer.writerow(('file', 'vertices', 'density-gen', 'edges', 'density',
                     'flow', 'elapsed'))
    for path, vertices, density in files:
        net = load_net(path)
        net.convert_to_adjlist()
        flow, elapsed = run_once(net, alg)
        edges = net.edge_count()
        density_val = edges / (net.size * (net.size - 1))
        writer.writerow(
            (path, vertices, density, edges, density_val, flow, elapsed))
        print((path, vertices, density, edges, density_val, flow, elapsed))


n = load_net('data/v500-d0.95-1.csv')

n.convert_to_adjlist()
print("exact", run_once(n, exact.exact_flow))
print("sparse", run_once(n, approx.approx))
print("limited", run_once(n, lambda net: approx.exact_flow_limited(net, int(n.size/5))))

data = filter(lambda i: i[2] > 0.9, datasets.dataset)
with open("out2-exact-95.csv", "w") as fout:
    run(data, exact.exact_flow, fout)
data = filter(lambda i: i[2] > 0.9, datasets.dataset)
with open("out2-approx-95.csv", "w") as fout:
    run(data, approx.approx, fout)
data = filter(lambda i: i[2] == 0.5, datasets.dataset)
with open("out2-exact-50.csv", "w") as fout:
    run(data, exact.exact_flow, fout)
data = filter(lambda i: i[2] > 0.9, datasets.dataset)
with open("out2-approx-50.csv", "w") as fout:
    run(data, approx.approx, fout)
