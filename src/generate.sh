#!/usr/bin/env bash

wmin=1
wmax=10

echo "dataset=[" > datasets.py

for vertices in  100 `seq 500 500 3000`
do
    for i in 1 2 3
    do
        for density in 0.1 0.5 0.95
        do
            filename="data/v$vertices-d$density-$i.csv"
            python3 xhloze01.py -w -wmin $wmin -wmax $wmax -f csv -s -d -r $density -n $vertices > $filename
            echo "(\"$filename\", $vertices, $density)," >> datasets.py
            echo "(\"$filename\", $vertices, $density),"
        done
    done
done

echo "]" >> datasets.py